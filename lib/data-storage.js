const _Pouchdb = require('basic-pouchdb');
const Dataengine = require('dataengine');

module.exports = class DataStorage extends Dataengine {
  constructor (host, port, protocol, username, password) {
    super(host, port, protocol, username, password);
    this._pouchdb = new _Pouchdb();
  }

  /*                                              bussiness                                      */
  /**
   * 下载商家信息
   */
  insertBusiness ({dbname, username, password}) {
    let that = this;
    let params = {
      include: ['field_business_logo']
    };
    // 创建数据库
    let db = that._pouchdb.createDatabase(dbname);

    let promise = new Promise((resolve, reject) => {
      // 请求de business 资源
      that.business(params).then(_data => {
        // 判断服务器返回状态
        if (_data.statusCode === 200) {
          try {
            let data = JSON.parse(_data.data);
            // data 数据为null 直接 返回data 由调用者处理
            if (data === null)
              reject(data);
            // 处理 存储到数据库的信息
            let doc = {
              _id: data.data[0].attributes.id.toString(), // 服务器上存储的商家id 作为pouchdb _id
              id: data.data[0].attributes.id.toString(), // 服务器上存储的商家id
              uuid: data.data[0].attributes.uuid, // 服务器上存储的商家uuid
              title: data.data[0].attributes.label, // 商家titile
              summary: data.data[0].attributes.field_business_summary, // 商家简述
              field_business_machine_name: data.data[0].attributes.field_business_machine_name, // 商家简称
              logo: null, // 商家logo 初始化为null
              username: username, // 商家帐号
              password: password // 商家密码
            };

            // 解析 included 
            if (data.hasOwnProperty('included')) {
              for (let i in data.included) {
                // 判断是否包含商家logo 更改logo 数据
                if (data.included[i].type === 'file--file')
                  doc.logo = data.included[i].attributes.url;
              }
            }
            // 销毁数据库原有数据
            that._pouchdb.destroy(db).then(res => {
              let db = that._pouchdb.createDatabase(dbname);
              // 重新填入数据
              that._pouchdb.put(db, doc).then(res => {
                resolve(res);
              }, err => {
                reject(err);
              });
            }, err => {
              reject(err);
            });
          } catch (error) {
            reject(error);
          }
        } else {
          // 非 200 直接返回_data 由调用者处理
          reject(_data);
        }
      }, err => {
        reject(err);
      });
    });

    return promise;
  }

  /*                                              store                                      */
  /**
   * 根据商家id 插入门店列表
   */
  insertStoreByBid ({id, useid, dbname}) {
    let that = this;
    let params = {
      include: ['entity_id']
    };

    // 创建数据库
    let db = that._pouchdb.createDatabase(dbname);

    let promise = new Promise((resolve, reject) => {
      // 请求de store 信息
      that.store(params).then(_data => {
        // 判断服务器返回状态
        if (_data.statusCode === 200) {
          try {
            let data = JSON.parse(_data.data);
            if (data === null)
              // data 数据为null 直接 返回data 由调用者处理
              reject(data);
            let docs = [];
            // 处理 存储到数据库的信息
            for (let i in data.data) {
              let doc = {
                _id: data.included[i].attributes.uuid,
                bid: id,
                uuid: data.included[i].attributes.uuid,
                title: data.data[i].attributes.label,
                store_id: data.included[i].attributes.store_id,
                account: data.included[i].attributes.field_account,
                mail: data.included[i].attributes.mail,
                address: data.included[i].attributes.address.address_line1,
                breakfast: data.included[i].attributes.field_store_breakfast_time,
                lunch: data.included[i].attributes.field_store_lunch_time,
                dinner: data.included[i].attributes.field_store_dinner_time,
                telephone: data.included[i].attributes.field_store_telephone,
                state: data.included[i].attributes.field_store_state,
                isuse: false
              };
              // 发现 id 与正在使用的门店id相等时 则判定改门店为正在使用的门店
              if (doc._id == useid) // eslint-disable-line
                doc.isuse = true;
              docs.push(doc);
            }
            // 销毁现有数据库
            that._pouchdb.destroy(db).then(res => {
              if (docs.length == 0) // eslint-disable-line
                resolve(true);
              else {
                db = that._pouchdb.createDatabase(dbname);
                that._pouchdb.bulkDocs(db, docs).then(res => {
                  resolve(res);
                }, err => {
                  reject(err);
                });
              }
            }, err => {
              reject(err);
            });
          } catch (error) {
            reject(error);
          }
        } else {
          // 非 200 直接返回_data 由调用者处理
          reject(_data);
        }
      }, err => {
        reject(err);
      });
    });

    return promise;
  }

  /*                                              product                                      */

  /**
   * 根据商家id 插入产品信息
   * @param {String} id
   */
  insertProduct ({dbname}) {
    let that = this;
    let params = {
      include: ['entity_id', 'entity_id.variations', 'entity_id.field_product_vocabulary',
        'entity_id.stores', 'entity_id.variations.attribute_other_flavor',
        'entity_id.variations.attribute_size', 'entity_id.variations.attribute_spicy',
        'entity_id.variations.attribute_sweetness', 'entity_id.variations.attribute_temperature',
        'entity_id.variations.field_product_image']
    };

    let db = that._pouchdb.createDatabase(dbname);

    let promise = new Promise((resolve, reject) => {
      that.product(params).then(_data => {
        if (_data.statusCode === 200) {
          try {
            let data = JSON.parse(_data.data);
            // 判断服务器返回信息是否包含服务器错误信息 有则reject 服务器错误信息
            if (data === null)
              reject(data);
            let included = {}; // 存放 服务器返回的所有included
            let docs = []; // 存放 存储到数据库的信息

            // 将服务器返回的included 数组转化为 object
            included = that.convertincluded(data.included);
            //  循环 服务器返回的data 取值
            for (let j in data.data) {
              // product entity
              let entity = data.data[j].relationships.entity_id.data;
              let vocabulary = included[entity.type][entity.id].relationships.field_product_vocabulary.data;
              // 此处目前只使用一个分类
              if (vocabulary != null && vocabulary.length > 0) // eslint-disable-line
                vocabulary = included[included[entity.type][entity.id].relationships.field_product_vocabulary.data[0].type][included[entity.type][entity.id].relationships.field_product_vocabulary.data[0].id].attributes.tid;
              // 数据库 单个产品信息
              let doc = {
                _id: data.data[j].attributes.id.toString(),  // 产品id 
                uuid: data.data[j].id, // 产品uuid
                label: included[entity.type][entity.id].attributes.title,  //  产品lable
                summary: included[entity.type][entity.id].attributes.field_product_summary,  // 产品简述
                weight: included[entity.type][entity.id].attributes.field_weight, // 产品权重
                status: included[entity.type][entity.id].attributes.status, // 产品状态
                product_id: included[entity.type][entity.id].attributes.product_id, // 产品productid
                field_background_color: included[entity.type][entity.id].attributes.field_background_color, // 背景色
                field_font_color: included[entity.type][entity.id].attributes.field_font_color, // 字体色
                field_kitchen_printer: included[entity.type][entity.id].attributes.field_kitchen_printer, // 打印厨房
                vocabulary: vocabulary, // 产品所属分类id
                stores: [], // 产品所属门店集合
                variations: [] // 子产品集合
              };
              // 产品所属门店
              let stores = included[entity.type][entity.id].relationships.stores.data;

              for (let k in stores) {
                doc.stores.push(included[stores[k].type][stores[k].id].attributes.store_id);
              }

              // 产品子产品
              let variations = included[entity.type][entity.id].relationships.variations.data;

              for (let x in variations) {
                let _product = included[variations[x].type][variations[x].id];
                let _child = {
                  variation_id: _product.attributes.variation_id,
                  uuid: _product.attributes.uuid,
                  sku: _product.attributes.sku,
                  title: _product.attributes.title,
                  status: _product.attributes.status,
                  price: _product.attributes.price,
                  other_flaor: [],
                  size: {},
                  spicy: [],
                  sweetness: [],
                  temperature: [],
                  product_image: ''
                };
                if (x == 0) // eslint-disable-line
                  doc.price = _child.price;

                // 其他口味
                let other_flaor = _product.relationships.attribute_other_flavor.data;

                for (let v in other_flaor) {
                  let flaor = {
                    id: included[other_flaor[v].type][other_flaor[v].id].attributes.attribute_value_id,
                    name: included[other_flaor[v].type][other_flaor[v].id].attributes.name,
                    weight: included[other_flaor[v].type][other_flaor[v].id].attributes.weight
                  };
                  _child.other_flaor.push(flaor);
                }

                // 规格
                let attribute_size = _product.relationships.attribute_size.data;

                if (attribute_size !== null) {
                  let size = {
                    id: included[attribute_size.type][attribute_size.id].attributes.attribute_value_id,
                    name: included[attribute_size.type][attribute_size.id].attributes.name,
                    weight: included[attribute_size.type][attribute_size.id].attributes.weight
                  };
                  _child.size = size;
                }

                // 辣度
                let attribute_spicy = _product.relationships.attribute_spicy.data;

                for (let v in attribute_spicy) {
                  let spicy = {
                    id: included[attribute_spicy[v].type][attribute_spicy[v].id].attributes.attribute_value_id,
                    name: included[attribute_spicy[v].type][attribute_spicy[v].id].attributes.name,
                    weight: included[attribute_spicy[v].type][attribute_spicy[v].id].attributes.weight
                  };
                  _child.spicy.push(spicy);
                }

                // 甜度
                let attribute_sweetness = _product.relationships.attribute_sweetness.data;

                for (let v in attribute_sweetness) {
                  let sweetness = {
                    id: included[attribute_sweetness[v].type][attribute_sweetness[v].id].attributes.attribute_value_id,
                    name: included[attribute_sweetness[v].type][attribute_sweetness[v].id].attributes.name,
                    weight: included[attribute_sweetness[v].type][attribute_sweetness[v].id].attributes.weight
                  };
                  _child.sweetness.push(sweetness);
                }

                // 温度
                let attribute_temperature = _product.relationships.attribute_temperature.data;

                for (let v in attribute_temperature) {
                  let temperature = {
                    id: included[attribute_temperature[v].type][attribute_temperature[v].id].attributes.attribute_value_id,
                    name: included[attribute_temperature[v].type][attribute_temperature[v].id].attributes.name,
                    weight: included[attribute_temperature[v].type][attribute_temperature[v].id].attributes.weight
                  };
                  _child.temperature.push(temperature);
                }

                // 图片
                let field_product_image = _product.relationships.field_product_image.data;

                if (field_product_image !== null)
                  _child.product_image = included[field_product_image.type][field_product_image.id].attributes.url;

                doc.variations.push(_child);
              }
              docs.push(doc);
            }
            that._pouchdb.destroy(db).then(res => {
              if (docs.length == 0) // eslint-disable-line
                resolve(true);
              else {
                db = that._pouchdb.createDatabase(dbname);
                that._pouchdb.bulkDocs(db, docs).then(res => {
                  resolve(res);
                }, err => {
                  reject(err);
                });
              }
            }, err => {
              reject(err);
            });
          } catch (error) {
            reject(error);
          }
        } else {
          reject(_data);
        }
      }, err => {
        reject(err);
      });
    });
    return promise;
  }

  /*                                              vocabulary                                      */

  /**
   * 根据用户id 插入分类信息
   * @param {String} id
   */
  insertVocabularyByuid ({uid, dbname}) {
    let that = this;
    let params = {
      include: ['entity_id']
    };

    let db = that._pouchdb.createDatabase(dbname);

    let promise = new Promise((resolve, reject) => {
      that.vocabulary(params).then(_data => {
        if (_data.statusCode === 200) {
          try {
            let data = JSON.parse(_data.data);
            if (data === null)
              reject(data);
            if (data.hasOwnProperty('included')) {
              if (data.included.length <= 0)
                reject(data);
              let path = '/jsonapi/taxonomy_term/';
              for (let i in data.included) {
                if (data.included[i].type === 'taxonomy_vocabulary--taxonomy_vocabulary')
                  path += data.included[i].attributes.vid;
                // path += 'zkf_vocab';
              }
              that.taxonomy({path: path}).then(res => {
                if (res.statusCode === 200) {
                  let docs = [];
                  let data = JSON.parse(res.data);
                  for (let i in data.data) {
                    let doc = {
                      _id: data.data[i].attributes.tid.toString(),
                      uuid: data.data[i].attributes.uuid,
                      title: data.data[i].attributes.name,
                      weight: data.data[i].attributes.weight,
                      parent: data.data[i].relationships.parent.data,
                      description: data.data[i].attributes.description
                    };
                    docs.push(doc);
                  }
                  that._pouchdb.destroy(db).then(res => {
                    if (docs.length == 0) // eslint-disable-line
                      resolve(true);
                    else {
                      db = that._pouchdb.createDatabase(dbname);
                      that._pouchdb.bulkDocs(db, docs).then(res => {
                        resolve(res);
                      }, err => {
                        reject(err);
                      });
                    }
                  }, err => {
                    reject(err);
                  });
                } else
                  reject(res);
              }, err => {
                reject(err);
              });
            } else
              reject(false);
          } catch (error) {
            reject(error);
          }
        } else {
          reject(_data);
        }
      }, err => {
        reject(err);
      });
    });
    return promise;
  }

  /**
   * 插入用户信息
   */
  insertUser ({dbname, username, password}) {
    let that = this;
    let params = {
      filter: {
        name: username
      }
    };
    let db = that._pouchdb.createDatabase(dbname);

    let promise = new Promise((resolve, reject) => {
      that.user(params).then(_data => {
        if (_data.statusCode === 200) {
          try {
            let data = JSON.parse(_data.data);
            if (data === null)
              reject(data);
            let doc = {
              _id: data.data[0].attributes.uid.toString(),
              uuid: data.data[0].attributes.uuid,
              name: data.data[0].attributes.name,
              username: username,
              password: password
            };
            //
            // let id = doc._id;
            // that.findTableByid({dbname, id}).then(res => {
            //   if (res.hasOwnProperty('_id')) {
            //     that.updateTable({doc, dbname}).then(res => {
            //       resolve(res);
            //     }, err => {
            //       reject(err);
            //     });
            //   } else {
            //     that._pouchdb.put(db, doc).then(res => {
            //       resolve(res);
            //     }, err => {
            //       reject(err);
            //     });
            //   }
            // }, err => {
            //   that._pouchdb.put(db, doc).then(res => {
            //     resolve(res);
            //   }, err => {
            //     reject(err);
            //   });
            // });
            that._pouchdb.destroy(db).then(res => {
              let db = that._pouchdb.createDatabase(dbname);
              that._pouchdb.put(db, doc).then(res => {
                resolve(res);
              }, err => {
                reject(err);
              });
            }, err => {
              reject(err);
            });
          } catch (err) {
            reject(err);
          }
        } else
          reject(_data);
      }, err => {
        reject(err);
      });
    });
    return promise;
  }

  /**
   * 插入优惠信息
   */
  insertPromotion ({dbname}) {
    let that = this;
    let params = {
      include: ['entity_id', 'entity_id.coupons', 'entity_id.stores']
    };

    let db = that._pouchdb.createDatabase(dbname);

    let promise = new Promise((resolve, reject) => {
      that.promotion(params).then(_data => {
        if (_data.statusCode === 200) {
          try {
            let data = JSON.parse(_data.data);
            if (data === null)
              reject(data);

            let included = {}; // 存放 服务器返回的所有included
            let docs = []; // 存放 存储到数据库的信息

            included = that.convertincluded(data.included);

            for (let j in data.data) {
              let entity = data.data[j].relationships.entity_id.data;

              let promotion = included[entity.type][entity.id];
              let doc = {
                _id: data.data[j].attributes.id.toString(),
                uuid: data.data[j].attributes.uuid,
                label: data.data[j].attributes.label,
                promotion: promotion.attributes,
                promotion_type: promotion.attributes.offer.target_plugin_id,
                coupons: [],
                stores: [],
                weight: promotion.attributes.weight
              };

              let stores = promotion.relationships.stores.data;

              for (let x in stores) {
                let store = included[stores[x].type][stores[x].id].attributes.store_id;
                doc.stores.push(store);
              }

              let coupons = promotion.relationships.coupons.data;

              for (let y in coupons) {
                let coupon = included[coupons[y].type][coupons[y].id].attributes;
                doc.coupons.push(coupon);
              }

              docs.push(doc);
            }

            that._pouchdb.destroy(db).then(res => {
              if (docs.length == 0) // eslint-disable-line
                resolve(true);
              else {
                db = that._pouchdb.createDatabase(dbname);
                that._pouchdb.bulkDocs(db, docs).then(res => {
                  resolve(res);
                }, err => {
                  reject(err);
                });
              }
            }, err => {
              reject(err);
            });
          } catch (err) {
            reject(err);
          }
        } else
          reject(_data);
      }, err => {
        reject(err);
      });
    });
    return promise;
  }

  /**
   * 插入员工职位信息
   */
  insertStorePerson ({id, dbname}) {
    let that = this;
    let params = {
      filter: {
        'first-filter][condition][path]=field_work_in_store.uuid&filter[first-filter][condition][operator]=IN&filter[first-filter][condition': id
      }
    };

    let db = that._pouchdb.createDatabase(dbname);

    let promise = new Promise((resolve, reject) => {
      that.membership(params).then(_data => {
        if (_data.statusCode === 200) {
          try {
            let data = JSON.parse(_data.data);
            if (data === null)
              reject(data);
            let docs = [];

            for (let i in data.data) {
              let doc = {
                _id: data.data[i].attributes.id.toString(),
                uuid: data.data[i].id,
                field_position: data.data[i].attributes.field_position,
                field_real_name: data.data[i].attributes.field_real_name,
                field_worker_pass: data.data[i].attributes.field_worker_pass,
                label: data.data[i].attributes.label
              };
              docs.push(doc);
            }

            that._pouchdb.destroy(db).then(res => {
              if (docs.length == 0) // eslint-disable-line
                resolve(true);
              else {
                db = that._pouchdb.createDatabase(dbname);
                that._pouchdb.bulkDocs(db, docs).then(res => {
                  resolve(res);
                }, err => {
                  reject(err);
                });
              }
            }, err => {
              reject(err);
            });
          } catch (err) {
            reject(err);
          }
        } else
          reject(_data);
      }, err => {
        reject(err);
      });
    });
    return promise;
  }

  /**
   * 插入套餐信息
   */
  insertProduct_bundle ({dbname}) {
    let that = this;
    let params = {
      include: ['entity_id', 'entity_id.bundle_items', 'entity_id.field_bundle_vocabulary', 'entity_id.stores', 'entity_id.bundle_items.product', 'entity_id.bundle_items.variations',
        'entity_id.bundle_items.variations.attribute_other_flavor',
        'entity_id.bundle_items.variations.attribute_size', 'entity_id.bundle_items.variations.attribute_spicy',
        'entity_id.bundle_items.variations.attribute_sweetness', 'entity_id.bundle_items.variations.attribute_temperature',
        'entity_id.bundle_items.variations.field_product_image']
    };

    let db = that._pouchdb.createDatabase(dbname);

    let promise = new Promise((resolve, reject) => {
      that.product_bundle(params).then(_data => {
        if (_data.statusCode === 200) {
          try {
            let data = JSON.parse(_data.data);
            if (data === null)
              reject(data);

            let included = {}; // 存放 服务器返回的所有included
            let docs = []; // 存放 存储到数据库的信息

            included = that.convertincluded(data.included);

            for (let i in data.data) {
              let entity = data.data[i].relationships.entity_id.data;
              // 套餐所属分类 目前只支持一个分类
              let vocabulary = included[entity.type][entity.id].relationships.field_bundle_vocabulary.data;
              // 此处目前只使用一个分类
              if (vocabulary != null)
                vocabulary = included[included[entity.type][entity.id].relationships.field_bundle_vocabulary.data[0].type][included[entity.type][entity.id].relationships.field_bundle_vocabulary.data[0].id].attributes.tid;
              let doc = {
                _id: data.data[i].attributes.id.toString(),  // id
                id: data.data[i].attributes.id.toString(),  // id
                uuid: data.data[i].attributes.uuid,  // uuid
                label: included[entity.type][entity.id].attributes.title, // 套餐名称
                weight: included[entity.type][entity.id].attributes.field_weight, // 套餐权重
                bundle_price: included[entity.type][entity.id].attributes.bundle_price, // 套餐价格
                bundle_id: included[entity.type][entity.id].attributes.id.toString(), // 套餐id
                sku: included[entity.type][entity.id].attributes.sku, // 套餐sku
                body: included[entity.type][entity.id].attributes.body, // 套餐介绍
                vocabulary: vocabulary,
                bundle_items: [], // 套餐items
                stores: [], // 套餐所属门店
                status: included[entity.type][entity.id].attributes.status // 套餐状态
              };
              // 套餐所属门店
              let stores = included[entity.type][entity.id].relationships.stores.data;

              for (let k in stores) {
                doc.stores.push(included[stores[k].type][stores[k].id].attributes.store_id);
              }

              // 套餐items
              let bundle_items = included[entity.type][entity.id].relationships.bundle_items.data;

              // 循环处理套餐items
              for (let i in bundle_items) {
                // 从 included 取出 items 详细
                let _item = included[bundle_items[i].type][bundle_items[i].id];
                let item = _item.attributes;

                // items 产品
                let _product = _item.relationships.product.data;

                // 判断套餐是否包含产品
                if (_product !== null) {
                  let product = included[_product.type][_product.id];
                  // console.log(product);

                  item['product'] = product.attributes;

                  // items variations
                  let _variations = _item.relationships.variations.data;
                  if (_variations.length > 0) {
                    let variations = included[_variations[0].type][_variations[0].id];

                    let _child = {
                      variation_id: variations.attributes.variation_id,
                      uuid: variations.attributes.uuid,
                      sku: variations.attributes.sku,
                      title: variations.attributes.title,
                      status: variations.attributes.status,
                      price: variations.attributes.price,
                      other_flaor: [],
                      size: {},
                      spicy: [],
                      sweetness: [],
                      temperature: [],
                      product_image: ''
                    };

                    // 其他口味
                    let other_flaor = variations.relationships.attribute_other_flavor.data;

                    for (let v in other_flaor) {
                      let flaor = {
                        id: included[other_flaor[v].type][other_flaor[v].id].attributes.attribute_value_id,
                        name: included[other_flaor[v].type][other_flaor[v].id].attributes.name,
                        weight: included[other_flaor[v].type][other_flaor[v].id].attributes.weight
                      };
                      _child.other_flaor.push(flaor);
                    }

                    // 规格
                    let attribute_size = variations.relationships.attribute_size.data;

                    if (attribute_size !== null) {
                      let size = {
                        id: included[attribute_size.type][attribute_size.id].attributes.attribute_value_id,
                        name: included[attribute_size.type][attribute_size.id].attributes.name,
                        weight: included[attribute_size.type][attribute_size.id].attributes.weight
                      };
                      _child.size = size;
                    }

                    // 辣度
                    let attribute_spicy = variations.relationships.attribute_spicy.data;

                    for (let v in attribute_spicy) {
                      let spicy = {
                        id: included[attribute_spicy[v].type][attribute_spicy[v].id].attributes.attribute_value_id,
                        name: included[attribute_spicy[v].type][attribute_spicy[v].id].attributes.name,
                        weight: included[attribute_spicy[v].type][attribute_spicy[v].id].attributes.weight
                      };
                      _child.spicy.push(spicy);
                    }

                    // 甜度
                    let attribute_sweetness = variations.relationships.attribute_sweetness.data;

                    for (let v in attribute_sweetness) {
                      let sweetness = {
                        id: included[attribute_sweetness[v].type][attribute_sweetness[v].id].attributes.attribute_value_id,
                        name: included[attribute_sweetness[v].type][attribute_sweetness[v].id].attributes.name,
                        weight: included[attribute_sweetness[v].type][attribute_sweetness[v].id].attributes.weight
                      };
                      _child.sweetness.push(sweetness);
                    }

                    // 温度
                    let attribute_temperature = variations.relationships.attribute_temperature.data;

                    for (let v in attribute_temperature) {
                      let temperature = {
                        id: included[attribute_temperature[v].type][attribute_temperature[v].id].attributes.attribute_value_id,
                        name: included[attribute_temperature[v].type][attribute_temperature[v].id].attributes.name,
                        weight: included[attribute_temperature[v].type][attribute_temperature[v].id].attributes.weight
                      };
                      _child.temperature.push(temperature);
                    }

                    // 图片
                    let field_product_image = variations.relationships.field_product_image.data;

                    if (field_product_image !== null)
                      _child.product_image = included[field_product_image.type][field_product_image.id].attributes.url;

                    let arr = [_child];
                    item.product['variations'] = [];
                    item.product['variations'] = arr;
                  }
                }
                let item_ = JSON.stringify(item);
                doc.bundle_items.push(JSON.parse(item_));
              }
              docs.push(doc);
            }
            that._pouchdb.destroy(db).then(res => {
              if (docs.length == 0) // eslint-disable-line
                resolve(true);
              else {
                db = that._pouchdb.createDatabase(dbname);
                that._pouchdb.bulkDocs(db, docs).then(res => {
                  resolve(res);
                }, err => {
                  reject(err);
                });
              }
            }, err => {
              reject(err);
            });
          } catch (err) {
            reject(err);
          }
        } else {
          reject(_data);
        }
      }, err => {
        reject(err);
      });
    });
    return promise;
  }

  /**
   * 查询 设备信息
   * @param {String} sid 门店id
   * @param {String} pid posid
   */
  findDevice ({sid, pid}) {
    let that = this;
    let params = {
      include: ['field_store'],
      filter: {
        field_pos_id: pid,
        'field_store.store_id': sid
      }
    };

    let promise = new Promise((resolve, reject) => {
      that.device(params).then(_data => {
        if (_data.statusCode === 200) {
          try {
            let data = JSON.parse(_data.data);
            if (data === null)
              reject(data);
            resolve(data);
          } catch (err) {
            reject(err);
          }
        } else
          reject(_data);
      }, err => {
        reject(err);
      });
    });
    return promise;
  }

  /**
   *  提交 设备信息
   * @param {Object} data 设备信息 
   */
  postDevice({data, method = 'POST', id = ''}) {
    let that = this;
    let params = {
      method: method,
      data: JSON.stringify(data),
      id: id
    };
    let promise = new Promise((resolve, reject) => {
      that.device(params).then(_data => {
        if (_data.statusCode === 201  || _data.statusCode === 200) {
          try {
            let data = JSON.parse(_data.data);
            if (data === null)
              reject(data);
            resolve(data);
          } catch (err) {
            reject(err);
          }
        } else
          reject(_data);
      }, err => {
        reject(err);
      });
    });
    return promise;
  }

  /**
   * 查询表信息
   */
  findTable ({options = {include_docs: true, attachments: true}, dbname}) {
    let that = this;
    let db = that._pouchdb.createDatabase(dbname);

    let promise = new Promise((resolve, reject) => {
      that._pouchdb.allDocs(db, options).then(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });

    return promise;
  }

  /**
   * 根据id 查询表信息
   */
  findTableByid ({dbname, id}) {
    let that = this;
    let db = that._pouchdb.createDatabase(dbname);
    let promise = new Promise((resolve, reject) => {
      that._pouchdb.get(db, id).then(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
    return promise;
  }

  /**
   * 删除表
   */
  deleteTable ({dbname}) {
    let that = this;
    let db = that._pouchdb.createDatabase(dbname);

    let promise = new Promise((resolve, reject) => {
      that._pouchdb.destroy(db).then(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });

    return promise;
  }

  /**
   * 修改表信息
   */
  updateTable ({doc, dbname}) {
    let that = this;
    let db = that._pouchdb.createDatabase(dbname);

    let promise = new Promise((resolve, reject) => {
      that._pouchdb.updateDoc(db, doc).then((result) => {
        resolve(result);
      }, (err) => {
        reject(err);
      });
    });

    return promise;
  }

  /**
   * 创建索引查询表信息
   */
  findTableByParams ({obj, fields, dbname}) {
    let that = this;
    let db = that._pouchdb.createDatabase(dbname);

    let promise = new Promise((resolve, reject) => {
      that._pouchdb.createIndex(db, fields).then((result) => {
        that._pouchdb.find(db, obj).then((result) => {
          resolve(result);
        }, (err) => {
          reject(err);
        });
      }, (err) => {
        reject(err);
      });
    });
    return promise;
  }

  // 转化include;
  convertincluded (array) {
    let included = {};
    for (let i in array) {
      if (included[array[i].type] === null || included[array[i].type] === undefined) {
        included[array[i].type] = {};
        included[array[i].type][array[i].id] = array[i];
      } else {
        included[array[i].type][array[i].id] = array[i];
      }
    }
    return included;
  }
};